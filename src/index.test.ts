import { TestParams, TestSuite } from "just-test-api";
import { changeContent } from "./index";
import { assert } from "chai";

export default function (suite: TestSuite): void {
	suite.test("Modules names are prefixed with the package name.", test => {
		test.arrange();
		let packageName = "myPackage";
		let mainModuleRegistration = "index";
		let input = `
define("ModuleA", function(){});
define("ModuleB", ["ModuleA"], function(){});
define("ModuleC", ["ModuleA", "ModuleB"], function(){});
`;
		let expected = `
define("${packageName}/ModuleA", function(){});
define("${packageName}/ModuleB", ["${packageName}/ModuleA"], function(){});
define("${packageName}/ModuleC", ["${packageName}/ModuleA", "${packageName}/ModuleB"], function(){});
`;

		test.act();
		let actual = changeContent(input, { name: packageName, mainModule: mainModuleRegistration });

		test.assert();
		assert.equal(actual, expected);
	});

	suite.test("When main module is the root, then change its name to the package name.", test => {
		test.arrange();
		let packageName = "myPackage";
		let mainModuleRegistration = "index";
		let input = `System.register("${mainModuleRegistration}",`;
		let expected = `System.register("${packageName}",`;

		test.act();
		let actual = changeContent(input, { name: packageName, mainModule: mainModuleRegistration });

		test.assert();
		assert.equal(actual, expected);
	});

	suite.describe("When main module in folder", suite => {
		suite.test("Main module parameter passed with front slash.", test => {
			test.arrange();
			let packageName = "myPackage";
			let mainModuleRegistration = "myFolder/index";
			let input = `System.register("${mainModuleRegistration}",`;
			let expected = `System.register("${packageName}",`;

			test.act();
			let actual = changeContent(input, { name: packageName, mainModule: mainModuleRegistration });

			test.assert();
			assert.equal(actual, expected);
		});

		suite.test("Main module parameter passed with back slash.", test => {
			test.arrange();
			let packageName = "myPackage";
			let mainModuleRegistration = "myFolder\index";
			let input = `System.register("${mainModuleRegistration}",`;
			let expected = `System.register("${packageName}",`;

			test.act();
			let actual = changeContent(input, { name: packageName, mainModule: mainModuleRegistration });

			test.assert();
			assert.equal(actual, expected);
		});

		suite.test("Main module starts with '_'.", test => {
			test.arrange();
			let packageName = "myPackage";
			let mainModuleRegistration = "myFolder\_index";
			let input = `System.register("${mainModuleRegistration}",`;
			let expected = `System.register("${packageName}",`;

			test.act();
			let actual = changeContent(input, { name: packageName, mainModule: mainModuleRegistration });

			test.assert();
			assert.equal(actual, expected);
		});
	});
}