export interface Options {
	name: string,
	mainModule?: string,
	minify?: MinifyOptions
}

export interface MinifyOptions {
	enabled: boolean,
	ignoredModules?: string[]
}
