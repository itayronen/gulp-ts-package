import { Options } from "./Options";

export interface InitializedOptions {
	name: string,
	mainModule: string | null,
	minify: InitializedMinifyOptions
}

export interface InitializedMinifyOptions {
	enabled: boolean,
	ignoredModules: string[],
	moduleCounter: number
}

export class InitializedOptions implements InitializedOptions {
	public name: string;
	public mainModule: string | null;
	public minify: InitializedMinifyOptions;

	constructor(options: Options) {
		this.name = options.name;
		this.mainModule = options.mainModule ? options.mainModule.replace("\\", "/") : null;

		this.minify = {
			enabled: false,
			ignoredModules: [],
			moduleCounter: 0
		};

		if (options.minify) {
			this.minify.enabled = options.minify.enabled;

			if (options.minify.ignoredModules) {
				this.minify.ignoredModules = options.minify.ignoredModules.map(e => e.replace("\\", "/"));
			}
		}
	}
}