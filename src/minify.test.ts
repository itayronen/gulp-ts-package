import { TestParams, TestSuite } from "just-test-api";
import { changeContent } from "./index";
import { assert } from "chai";

export default function (suite: TestSuite): void {
	suite.describe("Minify", suite => {
		suite.test("Non root modules names are prefixed with the package name and numbered." +
			" And ignoredModules names are not numbered.", test => {
				test.arrange();
				let packageName = "myPackage";
				let mainModuleRegistration = "index";
				let input = `
define("ModuleA", function(){});
define("ModuleB", ["ModuleA"], function(){});
define("ModuleC", ["ModuleA", "ModuleB"], function(){});
define("${mainModuleRegistration}", ["ModuleA", "ModuleB", "ModuleC"], function(){});
`;
				let expected = `
define("${packageName}/1", function(){});
define("${packageName}/ModuleB", ["${packageName}/1"], function(){});
define("${packageName}/2", ["${packageName}/1", "${packageName}/ModuleB"], function(){});
define("${packageName}", ["${packageName}/1", "${packageName}/ModuleB", "${packageName}/2"], function(){});
`;

				test.act();
				let actual = changeContent(input, {
					name: packageName,
					mainModule: mainModuleRegistration,
					minify: { enabled: true, ignoredModules: ["ModuleB"] }
				});

				test.assert();
				assert.equal(actual, expected);
			});
	});


	suite.test("When a module A name contains module B name, " +
		"and module C depends on A, then works. (Was a bug)", test => {
			test.arrange();
			let packageName = "myPackage";
			let mainModuleRegistration = "index";
			let input = `
define("ModuleA", function(){});
define("ModuleAB", function(){});
define("ModuleC", ["ModuleAB"], function(){});
`;
			let expected = `
define("${packageName}/1", function(){});
define("${packageName}/2", function(){});
define("${packageName}/3", ["${packageName}/2"], function(){});
`;

			test.act();
			let actual = changeContent(input, {
				name: packageName,
				minify: { enabled: true, ignoredModules: ["ModuleB"] }
			});

			test.assert();
			assert.equal(actual, expected);
		});
}