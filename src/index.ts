import ObjectStream, { EnteredArgs, EndedArgs, Transform } from "o-stream";
import * as File from "vinyl";
import { Options } from "./Options";
import { InitializedOptions } from "./InitializedOptions";

export { Options };

export default function plugin(options: Options): Transform {
	if (!options) {
		throw new Error("options parameter is empty.");
	}

	return ObjectStream.transform({
		onEntered: (args: EnteredArgs<File, File>) => {
			if (!args.object.contents) { args.output.push(args.object); return; }

			let newContent = changeContent(args.object.contents.toString(), options);

			args.object.contents = new Buffer(newContent);

			args.output.push(args.object);
		}
	});
}

export function changeContent(content: string, options: Options): string {
	return changeContentInitialized(content, new InitializedOptions(options));
}

type RenamedModule = { oldName: string, newName: string, defineLineIndex: number };
type Context = { options: InitializedOptions, newContentLines: string[], renamedModules: RenamedModule[] };

function changeContentInitialized(content: string, options: InitializedOptions): string {
	let contentLines: string[] = content.split("\n");
	let newContentLines: string[] = [];

	let renamedModules: RenamedModule[] = [];

	let context: Context = {
		options: options,
		newContentLines: newContentLines,
		renamedModules: renamedModules
	};

	for (let line of contentLines) {
		handleLine(line, context);
	}

	return newContentLines.join("\n");
}

function handleLine(line: string, context: Context): void {
	let defineString = getDefineStringOrNull(line);

	if (!defineString) {
		context.newContentLines.push(line);
		return;
	}

	let moduleName = getModuleName(line, defineString);
	let newModuleName = getNewModuleName(moduleName, context.options);

	line = line.replace(`"${moduleName}"`, `"${newModuleName}"`);

	for (let renamed of context.renamedModules) {
		line = line.replace(`"${renamed.oldName}"`, `"${renamed.newName}"`);
	}

	for (let moduleLineIndex of context.renamedModules.map(e => e.defineLineIndex)) {
		let line = context.newContentLines[moduleLineIndex];
		context.newContentLines[moduleLineIndex] = line.replace(`"${moduleName}`, `"${newModuleName}`);
	}

	context.renamedModules.push({
		defineLineIndex: context.newContentLines.length,
		oldName: moduleName,
		newName: newModuleName
	});

	context.newContentLines.push(line);
}

function getNewModuleName(moduleName: string, options: InitializedOptions): string {
	if (moduleName === options.mainModule) {
		return options.name;
	}
	else if (shouldMinifyModuleName(moduleName, options)) {
		return `${options.name}/${++options.minify.moduleCounter}`;
	}
	else {
		return `${options.name}/${moduleName}`;
	}
}

function shouldMinifyModuleName(moduleName: string, options: InitializedOptions): boolean {
	return options.minify.enabled && options.minify.ignoredModules.indexOf(moduleName) < 0;
}

function getModuleName(line: string, defineString: string): string {
	return /.+?(?=\")/.exec(line.substring(defineString.length))![0];
}

function getDefineStringOrNull(line: string): string | null {
	let systemRegister = "System.register(\"";
	let define = "define(\"";

	if (line.startsWith(systemRegister)) {
		return systemRegister;
	}
	else if (line.startsWith(define)) {
		return define;
	}
	else {
		return null;
	}
}