# gulp-ts-package
Renames modules as a package in a compiled typescript bundle (system or amd).

## Install
npm install --save-dev gulp-ts-package

## Usage
gulpfile.js
```js
let gulp = require("gulp");
let ts = require('gulp-typescript');
let tsPackage = require('gulp-ts-package').default;

gulp.task("build", function () {
	let tsProject = ts.createProject({ module: "amd", outFile: "bundle.amd.js" });

	return gulp.src("src/**/*.ts")
			.pipe(tsProject())
			// Assuming your main module is at: src/index.ts
			.pipe(tsPackage({ name: "my-library", mainModule: "index" }))
			.pipe(gulp.dest("lib/"));
		]);
});
```
### Minify module names
Enabling minify will change the name of non-root modules.  
The new name will be numbered after the package name. Example: "myPackage/1".

gulpfile.js
```js
let gulp = require("gulp");
let ts = require('gulp-typescript');
let tsPackage = require('gulp-ts-package').default;

gulp.task("build", function () {
	let tsProject = ts.createProject({ module: "amd", outFile: "bundle.amd.js" });

	return gulp.src("src/**/*.ts")
			.pipe(tsProject())
			// Assuming your main module is at: src/index.ts
			.pipe(tsPackage({
					name: "my-library",
					mainModule: "index",
					minify: {
						enabled: true,
						// Optional:
						ignoredModules: ["ModuleC"]
					}
				}))
			.pipe(gulp.dest("lib/"));
		]);
});
```

## How to bundle with typescript compiler?
Use the "outFile" parameter in the tsconfig.  
<https://www.typescriptlang.org/docs/handbook/compiler-options.html>

## Why bundle with typescript compiler?
If you are building a typescript library, you can:
* Avoid using bundling tools (webpack, rollup, systemjs builder, jspm, browserify).
* Avoid using transpilers (babel).
* Easily support all (or most) of the clients (node, browser, bundling tools, etc.).
* Works fast.
* Easier to maintain.

## Why is the module rename needed?
The original bundle file registers the modules by relative names, For example: "index".  
Problems:
1. Those relative names can easily collide with other modules - from your app or other libraries.
2. Absolute import (for example: `import * as myLib from "my-lib";`) will not work in the browser (out of the box).
	* For this to work, the main module in the bundle needs to be registered as the library name ("my-lib" in the example).

Notice that developers who uses bundling tools (like webpack), can avoid those issues by consuming and bundling the commonjs format of your library. The downside is performance.  

I personally try to devide my application to libraries when possible.  
That way I can enjoy smaller scope, faster builds and test runs, modularity, reusabilty, and more...  
All while keeping the maintanence relatively low (for example, short gulpfile.js).  
For this approach, this library is very helpful.

## Output Examples
You can see the source code of the files used in this example. The files are in the repository.

### System
#### Before
```
... // Some helper function might be here.

System.register("folder2/B", ["folder1/A"], function (exports_1, context_1) { ...

System.register("folder1/A", ["folder2/B"], function (exports_2, context_2) { ...

System.register("index", ["folder1/A", "folder2/B"], function (exports_3, context_3) { ...
```
#### After
```
... // Some helper function might be here.

System.register("my-library/folder2/B", ["my-library/folder1/A"], function (exports_1, context_1) { ...

System.register("my-library/folder1/A", ["my-library/folder2/B"], function (exports_2, context_2) { ...

System.register("my-library", ["my-library/folder1/A", "my-library/folder2/B"], function (exports_3, context_3) { ...
```

### AMD
#### Before
```
... // Some helper function might be here.

define("folder2/B", ["require", "exports", "folder1/A"], function (require, exports, A_1) { ...

define("folder1/A", ["require", "exports", "folder2/B"], function (require, exports, B_1) { ...

define("index", ["require", "exports", "folder1/A", "folder2/B"], function (require, exports, A_2, B_2) { ...
```
#### After
```
... // Some helper function might be here.

define("my-library/folder2/B", ["require", "exports", "my-library/folder1/A"], function (require, exports, A_1) { ...

define("my-library/folder1/A", ["require", "exports", "my-library/folder2/B"], function (require, exports, B_1) { ...

define("my-library", ["require", "exports", "my-library/folder1/A", "my-library/folder2/B"], function (require, exports, A_2, B_2) { ...
```
