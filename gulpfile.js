"use strict";
let gulp = require("gulp");
let ts = require('gulp-typescript');
let read = require("gulp-read");
let del = require("del");
let changed = require("itay-gulp-changed");
let merge = require("merge2");

let config = {
	tsconfig: "./src/tsconfig.json",
	tsGlob: "./src/**/*.ts",
	dest: "./lib",
};

gulp.task("build", function () {
	let tsProject = ts.createProject(config.tsconfig, { declaration: true });

	let tsResult = gulp.src(config.tsGlob, { read: false })
		.pipe(changed())
		.pipe(read())
		.pipe(tsProject());

	tsResult.on("error", () => {
		changed.reset();
		throw "Typescript build failed.";
	});

	return merge([
		tsResult.dts.pipe(gulp.dest(config.dest)),
		tsResult.js.pipe(gulp.dest(config.dest))
	]);
});

gulp.task("test", ["build"], () => {
	let tsPackage = require("./lib/index.js").default;
	let gutil = require("gulp-util");

	let tsProject = ts.createProject({
		module: "system",
		outFile: "bundle.system.js"
	});

	return gulp.src("./test/**/*.ts")
		.pipe(tsProject())
		.pipe(tsPackage({ name: "my-library", mainModule: "folder3\\index" }))
		.pipe(gutil.buffer(function (err, files) {
			let bundle = files[0].contents.toString();
			//  console.log(bundle);

			if (bundle.indexOf(`("my-library",`) < 0) throw "Failed.";
			if (bundle.indexOf(`["my-library/folder1/A", "my-library/folder2/B"]`) < 0) throw "Failed.";
			if (bundle.indexOf(`("my-library/folder1/A",`) < 0) throw "Failed.";
			if (bundle.indexOf(`("my-library/folder2/B",`) < 0) throw "Failed.";
		}));
});

gulp.task("clean", () => del([config.dest, "./.localStorage"]));