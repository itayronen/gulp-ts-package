import { B } from "../folder2/B";

export class A {
	public foo(): B {
		return new B();
	}
}